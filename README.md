GNU GENERAL PUBLIC LICENSE

Cette classe fournit des outils pour afficher des petits films à partir de frames en ascii.

Les frames peuvent être dans un seul fichier, avec un séparateur ou dans plusieurs fichiers qui seront lus d'un répertoire à préciser, et dans l'ordre alphanumérique croissant.

Pour démarrer, esayez

        from film.film import Film
        Film.demo()
    
- [Dépot](https://framagit.org/zenjo/film/tree/master)
- [Wiki](https://framagit.org/zenjo/film/wikis/home)
- [Documentation sphinx](https://docs.sebille.name/ascii-films/)
- [Bibliothèque de frames ascii](https://framagit.org/zenjo/film/wikis/Frames)



